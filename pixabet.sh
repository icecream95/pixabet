#!/bin/bash

source font_vars

mkdir -p temp

FONTSPECFILE=font

SFDTEMPLATE1='StartChar: '
SFDTEMPLATE2='
Encoding: '
SFDTEMPLATE3='
Width: '$FONW'
VWidth: 0
Flags: HW
LayerCount: 2
Fore
SplineSet
'
SFDTEMPLATE4='EndSplineSet
Validated: 1
EndChar
'
SFDTEMPLATE5=()
for i in {0..1}
do
    for j in {3..0}
    do
        SFDTEMPLATE5+=("$(($DISX+$PX*$i)) $(($DISY+$PY*$j)) m 1\n $(($DISX+$PX*$i)) $(($DISY+$PY*$j+$PH)) l 1\n $(($DISX+$PX*$i+$PW)) $(($DISY+$PY*$j+$PH)) l 1\n $(($DISX+$PX*$i+$PW)) $(($DISY+$PY*$j)) l 1\n $(($DISX+$PX*$i)) $(($DISY+$PY*$j)) l 1")
    done
done

echo -e 'SplineFontDB: 3.0
FontName: Pixabet
FullName: Pixabet
FamilyName: Pixabet
Weight: Regular
Copyright: Copyright (c) 2018, Klaatu Einzelganger
UComments: "2018-1-4: Created with FontForge (http://fontforge.org) on Slackware Linux"
Version: 004.000
ItalicAngle: 15
UnderlinePosition: -102
UnderlineWidth: 51
Ascent: '$(($FONH-$FOND))'
Descent: '$FOND'
InvalidEm: 0
LayerCount: 2
Layer: 0 0 "Back" 1
Layer: 1 0 "Fore" 0
XUID: [1021 33 -870393808 8134502]
StyleMap: 0x0000
FSType: 0
OS2Version: 0
OS2_WeightWidthSlopeOnly: 0
OS2_UseTypoMetrics: 1
CreationTime: 1514995565
ModificationTime: 1515027880
OS2TypoAscent: 0
OS2TypoAOffset: 1
OS2TypoDescent: 0
OS2TypoDOffset: 1
OS2TypoLinegap: 92
OS2WinAscent: 0
OS2WinAOffset: 1
OS2WinDescent: 0
OS2WinDOffset: 1
HheadAscent: 0
HheadAOffset: 1
HheadDescent: 0
HheadDOffset: 1
OS2Vendor: '\'PfEd\''
MarkAttachClasses: 1
DEI: 91125
LangName: 1033 "" "" "" "" "" "" "" "" "" "" "" "" "" "Copyright (c) 2018, Seth,666,, (<URL|email>),+AAoA-with Reserved Font Name Untitled2.+AAoACgAA-This Font Software is licensed under the SIL Open Font License, Version 1.1.+AAoA-This license is copied below, and is also available with a FAQ at:+AAoA-http://scripts.sil.org/OFL+AAoACgAK------------------------------------------------------------+AAoA-SIL OPEN FONT LICENSE Version 1.1 - 26 February 2007+AAoA------------------------------------------------------------+AAoACgAA-PREAMBLE+AAoA-The goals of the Open Font License (OFL) are to stimulate worldwide+AAoA-development of collaborative font projects, to support the font creation+AAoA-efforts of academic and linguistic communities, and to provide a free and+AAoA-open framework in which fonts may be shared and improved in partnership+AAoA-with others.+AAoACgAA-The OFL allows the licensed fonts to be used, studied, modified and+AAoA-redistributed freely as long as they are not sold by themselves. The+AAoA-fonts, including any derivative works, can be bundled, embedded, +AAoA-redistributed and/or sold with any software provided that any reserved+AAoA-names are not used by derivative works. The fonts and derivatives,+AAoA-however, cannot be released under any other type of license. The+AAoA-requirement for fonts to remain under this license does not apply+AAoA-to any document created using the fonts or their derivatives.+AAoACgAA-DEFINITIONS+AAoAIgAA-Font Software+ACIA refers to the set of files released by the Copyright+AAoA-Holder(s) under this license and clearly marked as such. This may+AAoA-include source files, build scripts and documentation.+AAoACgAi-Reserved Font Name+ACIA refers to any names specified as such after the+AAoA-copyright statement(s).+AAoACgAi-Original Version+ACIA refers to the collection of Font Software components as+AAoA-distributed by the Copyright Holder(s).+AAoACgAi-Modified Version+ACIA refers to any derivative made by adding to, deleting,+AAoA-or substituting -- in part or in whole -- any of the components of the+AAoA-Original Version, by changing formats or by porting the Font Software to a+AAoA-new environment.+AAoACgAi-Author+ACIA refers to any designer, engineer, programmer, technical+AAoA-writer or other person who contributed to the Font Software.+AAoACgAA-PERMISSION & CONDITIONS+AAoA-Permission is hereby granted, free of charge, to any person obtaining+AAoA-a copy of the Font Software, to use, study, copy, merge, embed, modify,+AAoA-redistribute, and sell modified and unmodified copies of the Font+AAoA-Software, subject to the following conditions:+AAoACgAA-1) Neither the Font Software nor any of its individual components,+AAoA-in Original or Modified Versions, may be sold by itself.+AAoACgAA-2) Original or Modified Versions of the Font Software may be bundled,+AAoA-redistributed and/or sold with any software, provided that each copy+AAoA-contains the above copyright notice and this license. These can be+AAoA-included either as stand-alone text files, human-readable headers or+AAoA-in the appropriate machine-readable metadata fields within text or+AAoA-binary files as long as those fields can be easily viewed by the user.+AAoACgAA-3) No Modified Version of the Font Software may use the Reserved Font+AAoA-Name(s) unless explicit written permission is granted by the corresponding+AAoA-Copyright Holder. This restriction only applies to the primary font name as+AAoA-presented to the users.+AAoACgAA-4) The name(s) of the Copyright Holder(s) or the Author(s) of the Font+AAoA-Software shall not be used to promote, endorse or advertise any+AAoA-Modified Version, except to acknowledge the contribution(s) of the+AAoA-Copyright Holder(s) and the Author(s) or with their explicit written+AAoA-permission.+AAoACgAA-5) The Font Software, modified or unmodified, in part or in whole,+AAoA-must be distributed entirely under this license, and must not be+AAoA-distributed under any other license. The requirement for fonts to+AAoA-remain under this license does not apply to any document created+AAoA-using the Font Software.+AAoACgAA-TERMINATION+AAoA-This license becomes null and void if any of the above conditions are+AAoA-not met.+AAoACgAA-DISCLAIMER+AAoA-THE FONT SOFTWARE IS PROVIDED +ACIA-AS IS+ACIA, WITHOUT WARRANTY OF ANY KIND,+AAoA-EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF+AAoA-MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT+AAoA-OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL THE+AAoA-COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,+AAoA-INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL+AAoA-DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING+AAoA-FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM+AAoA-OTHER DEALINGS IN THE FONT SOFTWARE." "http://scripts.sil.org/OFL"
Encoding: ISO8859-1
UnicodeInterp: none
NameList: AGL For New Fonts
DisplaySize: -48
AntiAlias: 1
FitToEm: 0
WinInfo: 44 22 10
BeginPrivate: 0
EndPrivate
BeginChars: 256 '$(($(cat $FONTSPECFILE | wc -l)+25))'
' >pixabet.sfd

CURN=0

function fontgen {
    while read NEXTLINE
    do
        echo -n "$SFDTEMPLATE1" >>pixabet.sfd
        echo -n "$NEXTLINE" | cut -d' ' -f1 | tr -d "\n" >>pixabet.sfd
        echo -n "$SFDTEMPLATE2" >>pixabet.sfd
        if [ "$(echo $NEXTLINE | tr -cd " " | wc -c)" -gt 1 ]; then
            echo -n $(echo -n $NEXTLINE | cut -d' ' -f2- | rev | cut -d' ' -f2- | rev | tr -d "\n") $(echo -n $NEXTLINE | cut -d' ' -f2- | rev | cut -d' ' -f2- | rev | tr -d "\n") $CURN >>pixabet.sfd
        else
            echo -n $(LC_CTYPE=C printf '%d' "'$(echo $NEXTLINE | head -c1)") $(LC_CTYPE=C printf '%d' "'$(echo $NEXTLINE | head -c1)") $CURN >>pixabet.sfd
        fi
        ((CURN++))
        echo -n "$SFDTEMPLATE3" >>pixabet.sfd
        DATA="$(echo $NEXTLINE | rev | cut -d' ' -f1 | rev)"
        for i in $(seq 1 ${#DATA})
        do
            if [ "X${DATA:i-1:1}" = "X1" ]; then
                echo -e "${SFDTEMPLATE5[$(($i-1))]}" >>pixabet.sfd
            fi
        done
        echo "$SFDTEMPLATE4" >>pixabet.sfd
    done
}

# Obviously not the best way to do it, but I can't be bothered doing it properly
SPLITLO="$(cat $FONTSPECFILE | grep -n LOWERCASE | cut -d':' -f1)"
SPLITLP="$(cat $FONTSPECFILE | grep -n SPACE | cut -d':' -f1)"
head -n $(( $SPLITLO - 1 )) $FONTSPECFILE >temp/font1temp
cat temp/font1temp | tr [A-Z] [a-z] >temp/font2temp
head -n $(( $SPLITLP - 1 )) $FONTSPECFILE | tail -n+$(( $SPLITLO + 1 )) >temp/font3temp
cat $FONTSPECFILE | tail -n+$(( $SPLITLP + 1 )) >temp/font4temp
fontgen <temp/font1temp
fontgen <temp/font2temp
fontgen <temp/font3temp
echo 'StartChar: space
Encoding: 32 32 '$CURN'
Width: '$FONW'
VWidth: 0
Flags: HW
LayerCount: 2
Fore
Validated: 1
EndChar
' >>pixabet.sfd
((CURN++))
fontgen <temp/font4temp

echo 'EndChars
EndSplineFont
' >>pixabet.sfd

rm -r temp
mkdir -p build
mkdir -p ~/.local/share/fonts

uudecode -o- $0 | python2

cp -i build/Pixabet.ttf ~/.local/share/fonts/Pixabet.ttf

exit 0

begin 644 convert.py
M(R$O=7-R+V)I;B]E;G8@<'ET:&]N,@H*:6UP;W)T(&9O;G1F;W)G90IP:7@]
M9F]N=&9O<F=E+F]P96XH(G!I>&%B970N<V9D(BD*<&EX+F=E;F5R871E*")B
3=6EL9"]0:7AA8F5T+G1T9B(I"@``
`
end
